<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title><?php print $head_title ?></title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <?php print $head ?>
    <?php print $styles ?>
  </head>

  <body class="<?php print($layout) ?>" <?php print theme("onload_attribute"); ?>>

    <div id="container">

      <table id="header" border="0">
        <tr>
          <?php if ($logo) : ?>
            <td id="logo">
              <a href="<?php print url() ?>" title="Index Page"><img src="<?php print($logo) ?>" alt="Logo" /></a>
            </td>
          <?php endif; ?>
          <td id="header-inner" valign="bottom">
            <?php if ($site_name): ?>
              <a id="site-name" href="<?php print url() ?>" title="Index Page"><?php print($site_name) ?></a>
            <?php endif;?>
            <?php if ($site_slogan): ?>
              <span id="site-slogan"><?php print($site_slogan) ?></span>
            <?php endif;?>
          </td>
        </tr>
      </table>

      <div class="main-content" id="content-<?php print $layout ?>">
        <?php if ($breadcrumb && $breadcrumb != "<div class=\"breadcrumb\"></div>"): ?>
          <?php print $breadcrumb ?>
        <?php endif; ?>

        <?php if ($mission != ""): ?>
          <div id="mission"><?php print $mission ?></div>
        <?php endif; ?>

        <div id="main-content-inner">

          <?php if ($title != ""): ?>
            <h1 class="content-title"><?php print $title ?></h1>
          <?php endif; ?>
          <?php if ($tabs != ""): ?>
            <?php print $tabs ?>
          <?php endif; ?>

          <?php if ($help != ""): ?>
            <div id="help"><?php print $help ?></div>
          <?php endif; ?>

          <?php if ($messages != ""): ?>
            <div id="message"><?php print $messages ?></div>
          <?php endif; ?>

          <!-- start main content -->
          <?php print($content) ?>
          <!-- end main content -->

        </div>

      </div><!-- mainContent -->

      <div id="sidebar">

        <?php if ($primary_links) : ?>
          <ul id="main-nav">
            <?php foreach ($primary_links as $link): ?>
              <li><?php print $link?></li>
            <?php endforeach; ?>
          </ul>
        <?php endif; ?>
        
        <?php if ($secondary_links) : ?>
          <ul id="sub-nav">
            <?php foreach ($secondary_links as $link): ?>
              <li><?php print $link?></li>
            <?php endforeach; ?>
          </ul>
        <?php endif; ?>

        <?php if ($search_box): ?>
          <form action="<?php print url("search") ?>" method="post">
            <div id="search">
              <input class="form-text" type="text" size="15" value="" name="edit[keys]" /><input class="form-submit" type="submit" value="<?php print t("Search")?>" />
            </div>
          </form>
        <?php endif; ?>

        <?php if ($sidebar_left != ""): ?>
          <div id="sidebar-left">
            <?php print $sidebar_left ?>
          </div>
        <?php endif; ?>
        
        <?php if ($sidebar_right != ""): ?>
          <div id="sidebar-right">
            <?php print $sidebar_right ?>
            <br class="clear" />
          </div>
        <?php endif; ?>

      </div>

      <div id="footer">
        <?php if ($footer_message) : ?>
          <p><?php print $footer_message;?></p>
        <?php endif; ?>
        <!-- p>Validate <a href="http://validator.w3.org/check/referer">XHTML</a> or <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>.</p -->
      </div><!-- footer -->

    </div>

    <?php print $closure;?>

  </body>

</html>
