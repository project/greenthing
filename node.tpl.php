<div class="node<?php print ($static) ? " static" : ""; ?>">

  <?php if ($page == 0): ?>
    <h2 class="node-title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>
  <?php print $picture ?>
  
  <div class="content">
    <?php print $content ?>
  </div>

  <div class="node-break">&nbsp;</div>
  
  <div class="node-footer">
    <div class="info">
      <?php print format_date($node->created) ?>
      <?php if ($terms): ?>
        <span class="terms">Filed under: <?php print $terms ?></span>
      <?php endif; ?>
    </div>

    <?php if ($links): ?>
      <div class="links"><?php print $links ?></div>
    <?php endif; ?>

  </div>

</div>
